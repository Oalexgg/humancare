const express = require("express");
const mongoose = require("mongoose");
const fileUpload = require("express-fileupload");
const app = express();
const port = process.env.PORT || 3000;
const www = process.env.WWW || "./";

const fileUpload_routes = require("./src/routes/form.routes");

app.use(express.static(www));
app.use(fileUpload());

console.log(`serving ${www}`);

app.get("*", (req, res) => {
  res.sendFile(`index.html`, { root: www });
});

app.use("/upload", fileUpload_routes);

app.listen(port, () => {
  mongoose.connect(
    "mongodb://localhost:27017/coding_challenge",
    {
      useNewUrlParser: true,
    },
    () => {
      console.log(`listening on http://localhost:${port}`);
      console.log(`MongoDB Connected`);
    }
  );
});
