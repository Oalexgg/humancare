# Coding Challenge

This repo contains the coding challenge.

## Installation

```bash
npm install
```

## Running

```bash
node server.js
```

## Testing

```bash
npm test
```

## Usage

Access your localhost on port 3000 to interact with the application.
