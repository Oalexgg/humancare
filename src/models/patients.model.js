const mongoose = require("mongoose");
const { Schema } = mongoose;
const Email = require("./emails.model");

const patientsSchema = new Schema({
  "Program Identifier": String,
  "Data Source": String,
  "Card Number": String,
  "Member ID": String,
  "First Name": String,
  "Last Name": String,
  "Date of Birth": String,
  "Address 1": String,
  "Address 2": String,
  City: String,
  State: String,
  "Zip code": String,
  "Telephone number": String,
  "Email Address": String,
  CONSENT: String,
  "Mobile Phone": String,
  emails: [{ type: Schema.Types.ObjectId, ref: "Emails" }],
});

patientsSchema.pre("save", async function () {
  if (this.CONSENT === "Y") {
    const emails = [];
    for (let index = 1; index < 5; index++) {
      const currentDate = new Date();
      const email = new Email({
        name: `Day ${index}`,
        scheduled_date: currentDate.setDate(currentDate.getDate() + index),
      });
      emails.push(await email.save());
    }
    this.emails = emails.map((email) => email._id);
  }
});
module.exports = mongoose.model("Patients", patientsSchema);
