const mongoose = require("mongoose");
const { Schema } = mongoose;

const emailSchema = new Schema({
  name: String,
  scheduled_date: String,
});

module.exports = mongoose.model("Emails", emailSchema);
