const path = require("path");
const fs = jest.createMockFromModule("fs");
const mongoose = require("mongoose");
const Patients = require("../models/patients.model");
const Emails = require("../models/emails.model");

jest.mock("fs");
describe("CodingChallenge", () => {
  beforeAll(async () => {
    const url = `mongodb://localhost:27017/coding_challenge`;
    await mongoose.connect(url, { useNewUrlParser: true });
  });

  it("Should verify the data in flat file matches the data in Patients collection", async () => {
    const patients = await Patients.find({});
    expect(patients).toHaveLength(18);
  });

  it("Should Print out all Patient IDs where the first name is missing", async () => {
    const patientsWithoutFirstName = await Patients.find({
      "First Name": { $lte: " " },
    });
    console.log(patientsWithoutFirstName);
    expect(patientsWithoutFirstName).toHaveLength(2);
  });

  it("Should Print out all Patient IDs where the email address is missing, but consent is Y", async () => {
    const patientsWitHConsent = await Patients.find({
      CONSENT: "Y",
    }).populate("emails");
    const emailsInPatients = await patientsWitHConsent
      .map((patient) => patient.emails.length)
      .every((emails) => emails > 0);
    expect(emailsInPatients).toBeTruthy();
    expect(patientsWitHConsent).toHaveLength(8);
  });

  it(" Emails were created in Emails Collection for patients who have CONSENT as Y", async () => {
    const patient = new Patients({
      "Program Identifier": "00000001",
      "Data Source": "WEB 3RD PARTY",
      "Card Number": "000000000",
      "Member ID": "12245",
      "First Name": "LOAD",
      "Last Name": "TEST 18",
      "Date of Birth": "03/02/1969",
      "Address 1": "3100 S Ashley Drive",
      "Address 2": "",
      City: "Chandler",
      State: "AZ",
      "Zip code": "85286",
      "Telephone number": "",
      "Email Address": "test18@humancaresystems.com",
      CONSENT: "Y",
      "Mobile Phone": "6177504303",
    });
    const newPatientInDatabase = await patient.save();
    expect(newPatientInDatabase.emails).toHaveLength(4);
    await Patients.deleteOne({ "Program Identifier": "00000001" });
  });

  it("Should Verify emails for each patient are scheduled correctly.", async () => {
    const patientsWitHConsent = await Patients.find({
      CONSENT: "Y",
    }).populate("emails");
    const emailsInPatients = await patientsWitHConsent
      .map((patient) => patient.emails)
      .every((email) => new Date(email.scheduled_date) instanceof Date);
    expect(emailsInPatients).toBeTruthy();
    expect(patientsWitHConsent).toHaveLength(8);
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });
});
