const { Readable } = require("stream");
const csv = require("csv-parser");
const Patients = require("./../models/patients.model");
const fs = require("fs");

function uploadFile(req, res) {
  const results = [];

  const buffer = new Buffer.from(req.files.patients_file.data);
  const readable = new Readable();
  readable._read = () => {}; // _read is required but you can noop it
  readable.push(buffer);
  readable.push(null); //Fix to have the buffer shown in the code
  readable
    .pipe(csv({ separator: "|" }))
    .on("data", async (data) => results.push(new Patients(data)))
    .on("end", async () => {
      try {
        for (const patient of results) {
          await patient.save();
        }
      } catch (error) {
        console.error("Something went wrong saving the patients.", error);
      }
    });
  res.status(200).send("Success!");
}

module.exports = {
  uploadFile,
};
