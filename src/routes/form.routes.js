const router = require("express").Router();
const uploadFileController = require("./../controllers/upload-file.controller");

router.post("", uploadFileController.uploadFile);


module.exports = router;